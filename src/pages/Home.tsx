import React, { FC, ReactElement, useState, useEffect } from "react";
import { makeStyles, createStyles } from "@mui/styles";
import GraphUi from "../components/GraphUi";

const useStyles: any = makeStyles((theme: any) =>
  createStyles({
    root: {
      width: "100%",
      height: "100vh",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    container: {
      border: "2px solid #CCC083",
      height: 500,
      width: 1000,
      backgroundColor: "#FFF2CE",
      borderRadius: 10,
    },
  })
);
const Home: FC<{}> = (): ReactElement => {
  const classes = useStyles();
  const [graphData, setData] = useState(null) as any;

  useEffect(() => {
    async function fetchJson() {
      await fetch("data.json")
        .then(function (res) {
          return res.json();
        })
        .then(function (data) {
          // store Data in State Data Variable
          setData(data);
        })
        .catch(function (err) {
          console.log(err, " error");
        });
    }

    fetchJson();
  }, []);

  // the graph configuration, just override the ones you need
  const graphConfig = {
    nodeHighlightBehavior: true,
    node: {
      color: "#FED0D6",
      size: 1000,
      highlightStrokeColor: "#A98371",
      highlightFontSize: 25,
      fontSize: 20,
      labelPosition: "top",
    },
    link: {
      highlightColor: "#A98371",
      color: "#000",
      renderLabel: true,
      type: "CURVE_SMOOTH",
      fontSize: 15,
    },
    directed: true,
    width: 1000,
    height: 500,
  };

  const onClickNode = function (nodeId: any) {
    window.alert(`Clicked node ${nodeId}`);
  };

  const onClickLink = function (source: any, target: any) {
    window.alert(`Clicked link between ${source} and ${target}`);
  };
  return (
    <>
      <div className={classes.root}>
        <div className={classes.container}>
          <GraphUi
            graphData={graphData}
            graphConfig={graphConfig}
            onClickNode={onClickNode}
            onClickLink={onClickLink}
          />
        </div>
      </div>
    </>
  );
};

export default Home;
